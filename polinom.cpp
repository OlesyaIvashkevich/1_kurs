/*
��������� �����. ������ 13501/3
�������:               ����������� ����� ��������� (� ������������ �����)

��������� ������:      �����������, ����������, ����, �����, �����������,
                       ��������, ���������, ���������,
                       ������ �������� ��� ��������� �.
*/
#include <iostream>
#include <math.h>
#include "polinom.h"

using namespace std;



polinom::polinom(int nn) //�������
  { 
  n=nn; 
  arrKoef=new int[n+1]; 
  for(int i=0;i<=n;i++) 
  arrKoef[i]=0; 
  } 

polinom::polinom(const polinom& secondPolinom) //�����������
  { 
   n=secondPolinom.n;
   arrKoef=new int [n+1];   
  for(int i=0;i<=n;i++) 
  arrKoef[i]=secondPolinom.arrKoef[i]; 
  } 

polinom::~polinom(void) //����������
  { 
  delete []arrKoef;  
  } 

polinom polinom::operator+(const polinom& secondPolinom) const //�������� ���������
  { 
  int i;
  if (n>=secondPolinom.n) 
  {
  polinom temp(n); 
   for(int i=0;i<=secondPolinom.n;i++) 
  temp.arrKoef[i]=arrKoef[i]+secondPolinom.arrKoef[i];
   for(i=secondPolinom.n+1;i<=n;i++)
      temp.arrKoef[i]=arrKoef[i];
  return temp;
  }; 
  if (n<secondPolinom.n) 
  {
  polinom temp(secondPolinom.n); 
  for(int i=0;i<=n;i++) 
  temp.arrKoef[i]=arrKoef[i]+secondPolinom.arrKoef[i]; 
  for( i=n+1;i<=secondPolinom.n;i++) 
  temp.arrKoef[i]=secondPolinom.arrKoef[i]; 
  return temp;}; 
  } 

polinom polinom::operator-(const polinom& secondPolinom) const //��������� ���������
  { 
  int i;
  if (n>=secondPolinom.n) 
  {
  polinom temp(n); 
   for(int i=0;i<=secondPolinom.n;i++) 
  temp.arrKoef[i]=arrKoef[i]-secondPolinom.arrKoef[i];
   for(i=secondPolinom.n+1;i<=n;i++)
      temp.arrKoef[i]=arrKoef[i];
  return temp;
  }; 
  if (n<secondPolinom.n) 
  {
  polinom temp(secondPolinom.n); 
  for(int i=0;i<=n;i++) 
  temp.arrKoef[i]=arrKoef[i]-secondPolinom.arrKoef[i]; 
  for( i=n+1;i<=secondPolinom.n;i++) 
  temp.arrKoef[i]=secondPolinom.arrKoef[i]-2*secondPolinom.arrKoef[i]; 
  return temp;}; 
  } 

polinom polinom::operator*(const polinom& secondPolinom) const //��������� ���������
  { 
  polinom temp(n+secondPolinom.n); 
  for(int i=0;i<=n;i++) 
  {
  for(int j=0;j<=secondPolinom.n;j++) 
  temp.arrKoef[i+j]+=arrKoef[i]*secondPolinom.arrKoef[j]; } 
  return temp; 
  } 

polinom & polinom::operator =(const polinom& secondPolinom)//�������� ������������
  {
if(n>secondPolinom.n) 
  { 
  for (int i=secondPolinom.n;i<=n;i++) 
  arrKoef[i]=0; 
  }  
  for(int i=0;i<=n;i++) 
  arrKoef[i]=secondPolinom.arrKoef[i]; 
  return *this; 
  }

/*---------------------����--------------------------*/
std::istream& operator>>(std::istream& ist, polinom& secondPolinom) { //���� 
  int nn;
  int n;
  int nnn;
  int *arrKoef;
  ist >> nnn;
  secondPolinom.n=n=nn=nnn; 
  arrKoef =  new int  [n+1];  
  for(int i=0;i<=nn;i++) 
  {  
  ist >> secondPolinom.arrKoef[i]; 
  }; 
  return ist;
  }

/*---------------------�����--------------------------*/
std::ostream& operator<<(std::ostream& ist, const polinom& secondPolinom) { //�����
    int bb; 
    bb=secondPolinom.n;  
    while(secondPolinom.arrKoef[bb]==0&&bb>=0) 
        bb=bb-1; 
    if(bb<0) ist << 0 << std::endl; 
    else{ 
        if(bb==0) ist << secondPolinom.arrKoef[0]; 
        else{ 
            ist << secondPolinom.arrKoef[bb]<<"x^"<<bb; 
            bb=bb-1; 
            for(int i=bb;i>0;i--) 
                { 
                if (secondPolinom.arrKoef[i]<0) {ist<<secondPolinom.arrKoef[i];}; 
                if (secondPolinom.arrKoef[i]>0) {ist<<"+"<<secondPolinom.arrKoef[i];}; 
                if (secondPolinom.arrKoef[i]!=0) {ist<<"x^"<<i;}; 
                }; 
            if (secondPolinom.arrKoef[0]<0) ist<<secondPolinom.arrKoef[0]; 
            if (secondPolinom.arrKoef[0]>0) ist<<"+"<<secondPolinom.arrKoef[0]; 
            }
        }
    return ist;
    }


double polinom::value(double x) const//������� �������� ��� ����� �
  {
  double val=0;
  int v=0;
  for (int i=0,v=0; i<=n; i++,v++)
  {
  val += pow(x,v) * arrKoef[i];
  }
  return val;
  }



void main() 
{ 
   int m;//��� ����
   int x;
   int c; 
   int nn;//�������
   nn=1; 
   polinom firstPolinom(nn),secondPolinom(nn); 
   do{   //���� ������
cout<<"\n";
cout<<"Snachala Vvodite koef,potom vibiraete metod"<<endl;
cout<<"Dlya proverki ocherednogo metoda opyat' vvodite koef i potom vibiraite metod"<<endl;
cout<<"1.Vvod koef"<<endl; 
cout<<"2.Vivod polinoma"<<endl; 
cout<<"3.Slozhenie polinomov"<<endl; 
cout<<"4.Proizvedenie polinomov"<<endl;
cout<<"5.Vichitanie"<<endl;
cout<<"6.Raschet znacheniya"<<endl;
cout<<"7.Vichod"<<endl; 
cout<<"Viberete punkt meny"<<endl; 
cin>>m; 
switch(m) 
  { 
  case 1: 
   { 
   cout<<"vvedite stepen'"; 
   cin>>firstPolinom; 
   } 
   case 2: 
   { 
   cout<<firstPolinom;  
   break; 
   }; 
  case 3: //��������
   { 
  cout<<"vvedite stepen'"; 
   cin>>secondPolinom; 
   int k;
   if(firstPolinom.vern()>secondPolinom.vern())
      k=firstPolinom.vern();
   else
      k=secondPolinom.vern();
   polinom endPolinom(k);
   endPolinom=firstPolinom+secondPolinom;
   cout<<endPolinom;
   break; 
   }; 
   case 4: 
   { 
   cout<<"vvedite stepen'";  
   cin>>secondPolinom; 
   int k;
   k=secondPolinom.vern()+firstPolinom.vern();
   polinom endPolinom(k);
   endPolinom=secondPolinom*firstPolinom; 
   cout<<endPolinom;
   break; 
   }; 
   case 5://���������
   {
   cout<<"vvedite stepen'"; 
   cin>>secondPolinom;  
   int k;
   if(firstPolinom.vern()>secondPolinom.vern())
      k=firstPolinom.vern();
   else
      k=secondPolinom.vern();
   polinom endPolinom(k);
   cout<<firstPolinom<<endl;
   cout<<secondPolinom<<endl;
   endPolinom=firstPolinom-secondPolinom;
   cout<<endPolinom;
   break; 
   }
   case 6://���������� ��������
   {
   cin>>x;
   cout<<firstPolinom.value(x);
   break;
   }
   case 7: 
   {  
   return;
   }; 
  }
  } 
  while (true); 
  }