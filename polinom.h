class polinom 
  { 
  private: 
    int n; 
    int nn;
    int *arrKoef;
  public: 
   polinom (int nn); //Полином
   polinom (const polinom &secondPolinom); //Копирование
   ~polinom(void); //Деструктор
   double value(double) const;//Значение полинома при вводе х
   polinom operator+(const polinom &secondPolinom) const; //Оператор сложения
   polinom operator-(const polinom &secondPolinom) const; //Оператор вычитание
   polinom operator*(const polinom &secondPolinom) const; //Оператор умножения
   polinom & operator=(const polinom& secondPolinom);//Оператор присваивания

   friend std::ostream& operator<<(std::ostream& ost, const polinom &secondPolinom);//Ввод
   friend std::istream& operator>>(std::istream& ist, polinom &secondPolinom);//Вывод

   int vern(){return n;} //Возврат степени
  }; 
